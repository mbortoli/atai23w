(define (problem ten-cities)
  (:domain tsp-strips)
  (:objects zone1 zone2 zone3 zone4 )
  (:init (connected zone1 zone2)
(=(distance zone1 zone2) 1.4142135623730951)
(connected zone1 zone3)
(=(distance zone1 zone3) 3.0)
(connected zone1 zone4)
(=(distance zone1 zone4) 2.0)
(connected zone2 zone1)
(=(distance zone2 zone1) 1.4142135623730951)
(connected zone2 zone3)
(=(distance zone2 zone3) 2.23606797749979)
(connected zone2 zone4)
(=(distance zone2 zone4) 1.4142135623730951)
(connected zone3 zone1)
(=(distance zone3 zone1) 3.0)
(connected zone3 zone2)
(=(distance zone3 zone2) 2.23606797749979)
(connected zone3 zone4)
(=(distance zone3 zone4) 1.0)
(connected zone4 zone1)
(=(distance zone4 zone1) 2.0)
(connected zone4 zone2)
(=(distance zone4 zone2) 1.4142135623730951)
(connected zone4 zone3)
(=(distance zone4 zone3) 1.0)
(visited zone1)(in zone1)(not-visited zone2)
(not-visited zone3)
(not-visited zone4)
)
  (:goal (and (visited zone1)
(visited zone2)
(visited zone3)
(visited zone4)
))
    (:metric minimize (total-cost))
  )