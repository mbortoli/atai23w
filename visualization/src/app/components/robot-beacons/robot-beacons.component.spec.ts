import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RobotBeaconsComponent } from './robot-beacons.component';

describe('RobotBeaconsComponent', () => {
  let component: RobotBeaconsComponent;
  let fixture: ComponentFixture<RobotBeaconsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RobotBeaconsComponent]
    });
    fixture = TestBed.createComponent(RobotBeaconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
