package com.robot_communication.services;

import com.rcll.domain.MachineName;
import com.rcll.domain.MachineSide;
import com.rcll.robot.RobotTaskCreator;
import lombok.NonNull;
import org.robocup_logistics.llsf_msgs.AgentTasksProtos;
import org.robocup_logistics.llsf_msgs.GripsPrepareMachineProtos;
import org.robocup_logistics.llsf_msgs.GripsTasksProtos;
import org.robocup_logistics.llsf_msgs.TeamProtos;

import java.util.Random;

public class GripsRobotTaskCreator extends RobotTaskCreator {
    public GripsPrepareMachineProtos.GripsPrepareMachine createPrepareMachineTask(@NonNull Long robotId,
                                                                                  @NonNull String machineId,
                                                                                  @NonNull String side) {
        return GripsPrepareMachineProtos.GripsPrepareMachine.newBuilder()
                .setRobotId(robotId.intValue())
                .setMachineId(machineId)
                .setMachinePrepared(true)
                .setMachinePoint(side)
                .build();
    }

    public GripsTasksProtos.GripsTask createGraspingChallengeNew(@NonNull Long robotId) {
        return GripsTasksProtos.GripsTask.newBuilder()
                .setTaskId(new Random().nextInt())
                .setRobotId(robotId.intValue())
                .setTeamColor(TeamProtos.Team.MAGENTA)
                .setGraspingChallenge(true)
                .build();
    }

    @Override
    public AgentTasksProtos.AgentTask createMoveToMachineTask(@NonNull Long robotId,
                                                              @NonNull Integer taskId,
                                                              @NonNull MachineName machine,
                                                              @NonNull MachineSide side) {
        String sideStr = side.toString().toLowerCase();
        if (side == MachineSide.Slide || side == MachineSide.Shelf) {
            sideStr = MachineSide.Input.toString().toLowerCase();
        }
        return this.createMoveToWaypointTask(robotId, taskId, machine.getRawMachineName() + "_" + sideStr + "_move_base#" + side.toString().toLowerCase());
    }
}
