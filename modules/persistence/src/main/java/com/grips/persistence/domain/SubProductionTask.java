package com.grips.persistence.domain;

import com.rcll.domain.MachineSide;
import com.visualization.domain.VisualizationSubTask;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class SubProductionTask implements VisualizationSubTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
	private String name;

    private String machine;

    @Enumerated(EnumType.STRING)
    private TaskState state;

    private Long orderInfoId;
    private String requiredColor;

    private String optCode;

    private boolean isDemandTask;

    private Integer robotId;
    
    private Long startTime;

    private Long endTime;

    @Enumerated(EnumType.STRING)
    private TaskType type;

    @Enumerated(EnumType.STRING)
    private MachineSide side;

	@Lob
    private String stateLogging;

    public SubProductionTask() {
    }

    public SubProductionTask(String name, String machine, TaskState state, TaskType type, MachineSide side,
                             Long orderInfoId, String requiredColor, String optCode, boolean isDemandTask) {
		super();
		this.name = name;
		this.machine = machine;
		this.state = state;
		this.orderInfoId = orderInfoId;
		this.type = type;
		this.side = side;
		this.requiredColor = requiredColor;
		this.optCode = optCode;
		this.isDemandTask = isDemandTask;
	}

	public void setState(TaskState state, String stateLogging) {
		this.state = state;
		this.stateLogging += ("; " + stateLogging);
	}

	public enum TaskState {SUCCESS, FAILED, TBD, INWORK, ASSIGNED, SUCCESS_PENDING, ASSIGNED_EXPLORATION }

    public enum TaskType {GET, DELIVER, DUMMY, MOVE, BUFFER_CAP}
}
