package com.visualization;

import com.rcll.domain.Order;
import com.visualization.domain.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TeamServerVisualization {
    private VisualizationGameState gameState;
    private long timeStamp;
    private Integer timeFromPlan;
    private List<? extends VisualizationTask> activeProductTasks;
    private List<? extends VisualizationLock> activeLockParts;
    VisualizationBeaconSignal robot1;
    VisualizationBeaconSignal robot2;
    VisualizationBeaconSignal robot3;
    private List<? extends VisualizationExplorationZone> explorationZones;
    private List<Order> productOrders;
    private List<? extends VisualizationRing> rings;
    private List<? extends VisualizationObservation> observations;
    private String plan;
}
