package com.visualization.domain;

import com.rcll.domain.MachineSide;

public interface VisualizationSubTask {
    long getId();
    String getName();
    String getMachine();
    MachineSide getSide();
    Integer getRobotId();
}
