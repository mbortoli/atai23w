package com.visualization.domain;

import com.rcll.domain.MachineName;

public interface VisualizationObservation {
    Long getObservationId();
    long getRobotId();
    String getMachineZone();
    MachineName getMachineName();
    long getObservationTimeGame();
}
