package com.grips.robot;

import com.google.protobuf.GeneratedMessageV3;
import com.grips.robot.msg_handler.*;
import com.grips.scheduled_tasks.SendAllKnownMachinesTask;
import com.rcll.protobuf_lib.RobotConnections;
import com.rcll.robot.HandleRobotMessageThread;
import com.rcll.robot.IRobotMessageThreadFactory;
import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.GripsExplorationFoundMachinesProtos;
import org.robocup_logistics.llsf_msgs.GripsPrepareMachineProtos;
import org.springframework.stereotype.Service;
import com.grips.config.StartPositionsConfig;


import java.net.Socket;
import java.util.AbstractMap;

@Service
@CommonsLog
public class HandleRobotMessageThreadFactory implements IRobotMessageThreadFactory {
    private final PrsTaskMsgHandler prsTaskMsgHandler;
    private final SendAllKnownMachinesTask sendAllKnownMachines;
    private final RobotConnections robotConnections;
    private final BeaconMsgHandler beaconMsgHandler;
    private final PrepareMachineMsgHandler prepareMachineMsgHandler;

    private final MachineReadyAtOutputHandler machineReadyAtOutputHandler;
    private final FoundMachinesMsgHandler foundMachinesMsgHandler;

    public HandleRobotMessageThreadFactory(PrsTaskMsgHandler prsTaskMsgHandler,
                                           SendAllKnownMachinesTask sendAllKnownMachines,
                                           RobotConnections robotConnections,
                                           BeaconMsgHandler beaconMsgHandler,
                                           PrepareMachineMsgHandler prepareMachineMsgHandler,
                                           MachineReadyAtOutputHandler machineReadyAtOutputHandler,
                                           FoundMachinesMsgHandler foundMachinesMsgHandler) {
        this.prsTaskMsgHandler = prsTaskMsgHandler;
        this.sendAllKnownMachines = sendAllKnownMachines;
        this.robotConnections = robotConnections;
        this.beaconMsgHandler = beaconMsgHandler;
        this.prepareMachineMsgHandler = prepareMachineMsgHandler;
        this.machineReadyAtOutputHandler = machineReadyAtOutputHandler;
        this.foundMachinesMsgHandler = foundMachinesMsgHandler;
    }


    public HandleRobotMessageThread create(Socket _socket, AbstractMap.SimpleEntry<GeneratedMessageV3, byte[]> msg) {
        return new HandleRobotMessageThread(_socket, robotConnections,
                (robotId) -> this.sendAllKnownMachines.sendAllKnownMachines(Long.valueOf(robotId)), beaconMsgHandler,
                prsTaskMsgHandler, this::handleMsg, msg);
    }

    @SneakyThrows
    private boolean handleMsg(AbstractMap.SimpleEntry<GeneratedMessageV3, byte[]> msg) {
        if (msg.getKey() instanceof GripsPrepareMachineProtos.GripsPrepareMachine) {
            GripsPrepareMachineProtos.GripsPrepareMachine prepareMachine = GripsPrepareMachineProtos.GripsPrepareMachine.parseFrom(msg.getValue());
            this.prepareMachineMsgHandler.accept(prepareMachine);
            return true;
        }
        if (msg.getKey() instanceof GripsPrepareMachineProtos.GripsMachineReadyAtOutput) {
            GripsPrepareMachineProtos.GripsMachineReadyAtOutput readyAtOutput = GripsPrepareMachineProtos.GripsMachineReadyAtOutput.parseFrom(msg.getValue());
            machineReadyAtOutputHandler.accept(readyAtOutput);
            return true;
        }
        if (msg.getKey() instanceof GripsExplorationFoundMachinesProtos.GripsExplorationFoundMachines) {
            this.foundMachinesMsgHandler.accept(GripsExplorationFoundMachinesProtos.GripsExplorationFoundMachines.parseFrom(msg.getValue()));
            return true;
        }
        return false;
    }

}
