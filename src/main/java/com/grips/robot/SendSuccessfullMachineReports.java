package com.grips.robot;

import com.google.common.collect.Lists;
import com.grips.persistence.domain.ExplorationZone;
import com.grips.persistence.domain.MachineReport;
import com.grips.persistence.dao.MachineReportDao;
import com.grips.scheduler.GameField;
import com.rcll.refbox.RefboxClient;
import com.robot_communication.services.GripsRobotClient;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.robocup_logistics.llsf_msgs.ZoneProtos;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@CommonsLog
public class SendSuccessfullMachineReports {
    private final MachineReportDao machineReportDao;
    private final RefboxClient refboxClient;
    private final GameField gameField;
    private final GripsRobotClient robotClient;
    public SendSuccessfullMachineReports(MachineReportDao machineReportDao,
                                         RefboxClient refboxClient,
                                         GameField gameField,
                                         GripsRobotClient robotClient) {
        this.machineReportDao = machineReportDao;
        this.refboxClient = refboxClient;
        this.gameField = gameField;
        this.robotClient = robotClient;
    }

    public void sendSuccessfullMachineReports(int robotId) {
        int numberMachineReports = Lists.newArrayList(machineReportDao.findAll()).size();
        int currentPoints = refboxClient.getCurrentPoint();
        if ((numberMachineReports * 2) == currentPoints) {
            // up to now, all reports seem to be correct, so broadcast this information to current robot
            List<MachineInfoProtos.Machine> machines = new ArrayList<>();
            for (MachineReport mr : machineReportDao.findAll()) {
                MachineInfoProtos.Machine m = MachineInfoProtos.Machine.newBuilder()
                        .setName(mr.getName().getRawMachineName())
                        .setZone(ZoneProtos.Zone.valueOf(mr.getZone().toString()))
                        .setRotation(mr.getRotation()).build();
                machines.add(m);
                //System.out.println("sending successfully reported machine " + mr.getName() + " to robot");

                ExplorationZone zone = gameField.getZoneByName(mr.getZone().toString());
                if (zone != null) {
                    ExplorationZone mirroredZone = zone.getMirroredZone();

                    int rotation = gameField.rotateMachineIfNecessary((int) zone.getZoneNumber() / 10,
                            (int) zone.getZoneNumber() % 10, mr.getName(), mr.getRotation());

                    MachineInfoProtos.Machine m_mirrored = MachineInfoProtos.Machine.newBuilder()
                            .setName(mr.getName().mirror().getRawMachineName())
                            .setZone(ZoneProtos.Zone.valueOf(mirroredZone.getZoneName()))
                            .setRotation(rotation).build();
                    machines.add(m_mirrored);


                    //System.out.println("sending successfully reported machine " + m_name + " to robot");
                }
            }

            MachineInfoProtos.MachineInfo mi = MachineInfoProtos.MachineInfo.newBuilder().addAllMachines(machines).build();
            try {
                this.robotClient.sendToRobot(robotId, mi);
            } catch (NumberFormatException nfe) {
                log.error("Beaconsignal does not contain parseable robot id");
            }
        }
    }
}
