package com.grips.monitoring;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RobotIdRequest {
    private long robotId;
}
