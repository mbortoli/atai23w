package com.grips.tools;

import java.util.List;

public class GridMPS {

    private Node _input;
    private Node _output;
    private List<Node> _occupiedNodes;

    public GridMPS(Node input, Node output, List<Node> occupiedNodes) {
        _input = input;
        _output = output;
        _occupiedNodes = occupiedNodes;
    }

    public Node getInput() {
        return _input;
    }

    public void setInput(Node _input) {
        this._input = _input;
    }

    public Node getOutput() {
        return _output;
    }

    public void setOutput(Node _output) {
        this._output = _output;
    }

    public List<Node> getOccupiedNodes() {
        return _occupiedNodes;
    }

    public void setOccupiedNodes(List<Node> _occupiedNodes) {
        this._occupiedNodes = _occupiedNodes;
    }
}
