package com.grips.tools;

import java.awt.geom.Point2D;

public class Node {

    private int x;
    private int y;
    private boolean occupied;

    private double g = 0.0;
    private double f = 0.0;
    private double h = Double.MAX_VALUE;

    private boolean open = false;
    private boolean close = false;

    private Node parent;

    public void resetCosts() {
        parent = null;
        g = 0.0;
        f = 0.0;
        h = Double.MAX_VALUE;
        open = false;
        close = false;
    }

    public Node(int x, int y, boolean occupied) {
        this.x = x;
        this.y = y;
        this.occupied = occupied;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double getG() {
        return g;
    }

    public void setG(double g) {
        this.g = g;
    }

    public double getF() {
        return f;
    }

    public void setF(double f) {
        this.f = f;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public boolean isClose() {
        return close;
    }

    public void setClose(boolean close) {
        this.close = close;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

}
