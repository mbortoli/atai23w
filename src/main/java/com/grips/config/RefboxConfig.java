package com.grips.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "refbox")
@Getter
@Setter
public class RefboxConfig {
    private String ip;
    private int public_port_send;
    private int public_port_receive;
    private int private_port_send_cyan;
    private int private_port_receive_cyan;
    private int private_port_send_magenta;
    private int private_port_receive_magenta;
    private String crypto_key;
}
