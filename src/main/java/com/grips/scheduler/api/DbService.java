package com.grips.scheduler.api;

import com.grips.config.StationsConfig;
import com.grips.persistence.dao.LockPartDao;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.grips.tools.PathEstimator;
import com.rcll.domain.Ring;
import com.rcll.domain.RingColor;
import com.rcll.refbox.RefboxClient;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@CommonsLog
public class DbService {
    private final StationsConfig stationsConfig;

    private final RefboxClient refboxClient;
    private final PathEstimator pathEstimator;

    @Value("${gameconfig.planningparameters.minorderstoplan}")
    private int minOrdersToPlan;



    public DbService(StationsConfig stationsConfig, RefboxClient refboxClient, PathEstimator pathEstimator) {
        this.refboxClient = refboxClient;
        this.stationsConfig = stationsConfig;
        this.pathEstimator = pathEstimator;
    }

    public boolean checkProductionReady() {
        if (stationsConfig.isUseRs1() && stationsConfig.isUseRs2()) {
            if (refboxClient.getAllRings().size() < 4) {
                log.error("Rings missing!");
                return false;
            }
        } else {
            //todo add checking for only one RS!! now we can skip that!
        }
        if (pathEstimator.availableMachines() < stationsConfig.countUsedMachines()) {
            log.warn("Only: " + pathEstimator.availableMachines() + " machines in path estimator, waiting...");
            return false;
        }
        return true;
    }

    public boolean hasOrders() {
        if (refboxClient.getAllOrders().size() >= minOrdersToPlan) {
            return true;
        }
        log.error("Not yet enough Orders - having " + refboxClient.getAllOrders().size() + " needing at least: " + minOrdersToPlan + "!");
        return false;
    }
}
