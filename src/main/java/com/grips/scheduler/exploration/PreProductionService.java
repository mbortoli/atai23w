package com.grips.scheduler.exploration;

import com.rcll.domain.MachineName;
import com.grips.persistence.domain.WorldKnowledge;
import com.rcll.domain.MachineSide;
import com.robot_communication.services.GripsRobotTaskCreator;
import lombok.Getter;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.AgentTasksProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Random;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

@Service
@CommonsLog
@Getter
public class PreProductionService {
    private final GripsRobotTaskCreator robotTaskCreator;

    private final WorldKnowledge worldKnowledge;
    private final Map<MachineName, Boolean> usedMachiens;

    private final Map<Long, MachineName> prepareIs;
    private final Map<Long, Stack<AgentTasksProtos.AgentTask>> tasks;

    @Autowired
    public PreProductionService(GripsRobotTaskCreator robotTaskCreator) {
        this.robotTaskCreator = robotTaskCreator;
        usedMachiens = new ConcurrentHashMap<>();
        worldKnowledge = new WorldKnowledge();
        tasks = new ConcurrentHashMap<>();
        prepareIs = new ConcurrentHashMap<>();
    }

    public void handleMachineInfo(MachineInfoProtos.Machine machine) {
        MachineName machineName = new MachineName(machine.getName());
        if (machineName.isCapStation() && !this.usedMachiens.containsKey(machineName)) {
            log.info("Got new machine to use: " + machineName);
            this.usedMachiens.put(machineName, false);
        }
    }

    public MachineName getPrepare(Long robotId) {
        MachineName machine = prepareIs.get(robotId);
        prepareIs.remove(robotId);
        worldKnowledge.setRobotAtMachine(robotId, machine);
        if (machine.isCapStation1()) {
            worldKnowledge.setCs1Buffered(true);
        }
        else if (machine.isCapStation2()) {
            worldKnowledge.setCs2Buffered(true);
        }
        return machine;
    }

    public AgentTasksProtos.AgentTask getPreProductionTask(long robotId) {
        return null;
        /*
        log.info("Try to find preProduction task for " + robotId);
        if (tasks.containsKey(robotId) && !tasks.get(robotId).empty()) {
            log.info("Found PreProduction task to Finish!");
            AgentTasksProtos.AgentTask toReturn = tasks.get(robotId).pop();
            if (tasks.get(robotId).isEmpty()) {
                tasks.remove(robotId);
            }
            return toReturn;
        }
        for (MachineName machineName : usedMachiens.keySet()) {
            if (!usedMachiens.getOrDefault(machineName, true)) {
                if (machineName.isCapStation()) {
                    log.info("Creating PreProduction task at: " + machineName + " for robot: " + robotId);
                    usedMachiens.put(machineName, true);
                    Stack<AgentTasksProtos.AgentTask> taskStack = new Stack<>();
                    taskStack.push(robotTaskCreator.createBufferCapTask(robotId, new Random().nextInt(), machineName, 1));
                    taskStack.push(robotTaskCreator.createMoveToMachineTask(robotId, new Random().nextInt(), machineName, MachineSide.Input));
                    prepareIs.put(robotId, machineName);
                    tasks.put(robotId, taskStack);
                    return taskStack.pop();
                }
            }
        }
        log.info("Found no PreProductino task for: " + robotId);
        return null;
         */
    }

    public boolean anyTask() {
        tasks.forEach((a, t) -> log.info("Task: " + t));
        return !tasks.isEmpty();
    }
}
