package com.grips.scheduler.asp.dispatcher;


import com.grips.config.RefboxConfig;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.SubProductionTaskBuilder;
import com.rcll.domain.*;
import com.rcll.planning.encoding.IntOp;
import com.rcll.refbox.RefboxClient;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.MachineDescriptionProtos;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@CommonsLog
public class ActionMapping {

    private final RefboxClient refboxClient;
    private List<String> cpConstants;

    private String colorprefix;

    public ActionMapping(RefboxClient refboxClient, RefboxConfig refboxConfig) {
        this.refboxClient = refboxClient;
    }

    public void updateCodedProblem(List<String> cpConstants) {
        this.cpConstants = cpConstants;
    }

    public SubProductionTask actionToTask (IntOp action) {
        String colorprefix = refboxClient.getTeamColor().map(x -> TeamColor.CYAN.equals(x) ? "C-": "M-").orElseThrow();

        String name = action.getName();
        SubProductionTask task;
        int orderIdIndex;
        int orderId;
        int robotIndex =  action.getInstantiations()[0];
        int robotId = Character.getNumericValue(cpConstants.get(robotIndex).charAt(1));
        switch (name) {


            case "move":
                int stationIndex;
                String station;
                int locationIndex = action.getInstantiations()[2];
                String stationAndSide = cpConstants.get(locationIndex);
                String side = "";
                if (stationAndSide.equals("bs") || stationAndSide.equals("ds")) {
                    station = colorprefix + stationAndSide.toUpperCase(Locale.ROOT);
                    if (stationAndSide.equals("bs"))
                        side = "output";
                    else
                        side = "input";
                } else {
                    station = stationAndSide.substring(0, stationAndSide.indexOf('_'));
                    station = colorprefix + station.toUpperCase(Locale.ROOT);
                    side = stationAndSide.substring(stationAndSide.indexOf('_') + 1);
                }
                MachineSide mside;
                if (side.equals("input"))
                    mside = MachineSide.Input;
                else
                    mside = MachineSide.Output;

                if (action.getSpecialInput() == 1)
                    mside = MachineSide.Shelf;
                else if (action.getSpecialInput() == 2)
                    mside = MachineSide.Slide;

                task = SubProductionTaskBuilder.newBuilder()
                        .setName("Move")
                        .setMachine(station)
                        .setState(SubProductionTask.TaskState.INWORK)
                        .setType(SubProductionTask.TaskType.MOVE)
                        .setSide(mside)
                        .setOrderInfoId(null)
                        .setIsDemandTask(false)
                        .build();
                break;

            case "toyactiondeliveringfrombs":
                orderIdIndex =  action.getInstantiations()[1];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                Order order = refboxClient.getOrderById(orderId);
                Base baseColor = order.getBase();

                task = SubProductionTaskBuilder.newBuilder()
                        .setName("toy")
                        .setMachine(colorprefix + "BS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.Output)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setOrderInfoId((long) orderId)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setRequiredColor(baseColor.toString())
                        .setOptCode(null)
                        .build();
                break;


            //ATAI parameters that have to be set for specific kind of tasks:
            //.setOrderInfoId((long) orderId) in all tasks in which you are manipulating a subpiece of the product (buffering cs, retrieving the base from the CS after the buffering, retrieving a base for a RS and delivering a base to the RS slide are excluded)
            //.setIsDemandTask(true) for all the tasks excluded by .setOrderInfoId (buffering cs, retrieving the base from the CS after the buffering, retrieving a base for a RS and delivering a base to the RS slide)
            //.setRequiredColor for retrieving a base, delivering partial product to RS to mount a ring, delivering partial product to CS to mount a cap
            //.setOptCode("RETRIEVE_CAP") for the buffer cap action
            //.setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString()) when delivering a partial product to a CS to mount a cap
            //.setOptCode(order.getDeliveryGate()+"") when delivering the final product to the DS (order is a ProductOrder object stored in the ProductOrderDAO)




            default:
                System.out.println("case not managed");
                task = null;
                break;
        }
        task.setRobotId(robotId);
        return task;
    }
}
