package com.grips.scheduler.asp;

import com.grips.config.ProductionConfig;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.Atom;
import com.grips.persistence.domain.WorldKnowledge;
import com.rcll.domain.Order;
import com.rcll.domain.ProductComplexity;
import com.rcll.domain.RingColor;
import com.rcll.planning.encoding.IntOp;
import com.rcll.planning.encoding.RcllCodedProblem;
import com.rcll.planning.util.TempAtom;
import com.rcll.refbox.RefboxClient;
import fr.uga.pddl4j.util.IntExp;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

@CommonsLog
@Service
public class KnowledgeBase {

    private final AtomDao atomDao;
    @Value("${gameconfig.planningparameters.numberofrobots}")
    private int numberRobots;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private RcllCodedProblem cp;


    @Autowired
    private ProductionConfig productionConfig;

    @Value("${gameconfig.productiontimes.normalTask}")
    private int normalTaskDuration;
    @Value("${gameconfig.productiontimes.bufferTask}")
    private int bufferTaskDuration;
    @Value("${gameconfig.productiontimes.moveTask}")
    private int moveTaskDuration;

    private final RefboxClient refboxClient;
    public  KnowledgeBase(AtomDao atomDao, MachineInfoRefBoxDao machineInfoRefBoxDao, ProductionConfig productionConfig,
                          RefboxClient refboxClient) {
        this.atomDao = atomDao;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.productionConfig = productionConfig;
        this.refboxClient = refboxClient;
    }

    //ATAI: define the initial atoms of the KB here
    public void generateInitialKB () {
        //generation of standard atom (robots, stations,  ecc...):
        this.add(new Atom("at", "r1;start"));
        this.add(new Atom("free", "r1"));
        this.add(new Atom("n_holding", "r1"));
        this.add(new Atom("empty", "bs"));

    }

    private String ringToColorInPddl(RingColor ringColor) {
        switch (ringColor) {
            case Blue:
                return "ring_blue";
            case Green:
                return "ring_green";
            case Orange:
                return "ring_orange";
            case Yellow:
                return "ring_yellow";
        }
        throw new IllegalArgumentException("Unkown ring color: " + ringColor);
    }

    //ATAI: given a set of goal chosen by the goalreasoner, model the related KB atoms here
    public void generateOrderKb (List<Order> goals) {
        for (Order p : goals) {
            long id = p.getId();
            this.add(new Atom("stage_c0_0", "p" + id));

        }
    }


    public void printKB () {
        for( Atom at : atomDao.findAll())
            System.out.println(at.toString());
    }


    public void prettyPrintKBtoProblemFile (FileWriter fr) throws IOException {
        for (Atom at : atomDao.findAll())
            if(!at.getName().equals("holdafter") )
                fr.write(at.prettyPrinter());
    }


    public void add (Atom at) {
        if (atomDao.findByNameAndAttributes(at.getName(), at.getAttributes()).size() == 0 )
            atomDao.save(at);
        else
            log.warn("Trying to add to KB atom " + at.toString() + " already present");

    }



    public boolean isNotInKB (String pd, String attributes) {
        return atomDao.findByNameAndAttributes(pd, attributes).isEmpty();
    }

    public void setCp (RcllCodedProblem cp) {
        this.cp = cp;
    }


    public String intToString (int number) {
        switch (number) {

            case 0:
                return "zero";

            case 1:
                return "one";

            case 2:
                return "two";

            case 3:
                return "three";

            default:
                return null;

        }
    }


    public void purgeKB () {
        atomDao.deleteAll();
    }







    /**
     * check if end precondition of action are satisfied
     * @param action
     * @return
     */



}
