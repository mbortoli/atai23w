package com.grips.team_server;

import com.grips.scheduler.challanges.NavigationScheduler;
import lombok.extern.apachecommons.CommonsLog;
import org.junit.jupiter.api.Test;
import org.robocup_logistics.llsf_msgs.NavigationChallengeProtos;
import org.robocup_logistics.llsf_msgs.ZoneProtos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.robocup_logistics.llsf_msgs.TeamProtos.Team.*;

@CommonsLog
public class NavigationSchedulerTest {

    @Test
    public void processKillTest() {
        /*
        try {
            for (int i = 0; i < 15; i++) {
                Process p = Runtime.getRuntime().exec("/usr/bin/sleep 1000");
                log.info("Info1: " + p);
                p.destroy();
                log.info("Info2: " + p);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }*/
    }
    void doSomething() {
        int fail_counter = 0;
        //BeaconSignalProtos.BeaconSignal.
            Random rand = new Random();
            List<ZoneProtos.Zone> zones = new ArrayList<ZoneProtos.Zone>();
            while (zones.size() < 12) {
                int dec = 1 + rand.nextInt(5);
                int one = 1 + rand.nextInt(5);
                if (dec * 10 + one == 51 || dec * 10 + one == 41 || dec * 10 + one == 31)
                    continue;
                if (zones.contains(ZoneProtos.Zone.forNumber(1000 + dec * 10 + one)))
                    continue;
                zones.add(ZoneProtos.Zone.forNumber(1000 + dec * 10 + one));
            }
            Iterable<ZoneProtos.Zone> iterable = zones;
            System.out.println("Hell0");
            NavigationChallengeProtos.Route r = NavigationChallengeProtos.Route.newBuilder().addAllRoute(iterable).setId(0).build();
            NavigationScheduler nav = new NavigationScheduler(null, null);
            NavigationChallengeProtos.NavigationRoutes route = NavigationChallengeProtos.NavigationRoutes.newBuilder()
                    .setTeamColor(MAGENTA)
                    .addRoutes(r)
                    .build();


            //System.out.println(route.getRoutesList().get(0).getRouteList().size());

            //nav.updateRoutes(route);
            Boolean ret = nav.clusterRoutes();
            if(!ret)
                fail_counter++;

        //System.out.println(fail_counter);
    }
}
