package com.grips;

import com.grips.robot.msg_handler.ReportMachineMsgHandler;
import com.rcll.refbox.RefboxClient;
import org.junit.jupiter.api.Test;
import org.robocup_logistics.llsf_msgs.GripsExplorationReportMachineProtos;
import org.robocup_logistics.llsf_msgs.MachineReportProtos;
import org.robocup_logistics.llsf_msgs.ZoneProtos;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReportMachineTest {
    @Test
    public void testMirroring() {
        RefboxClient refboxClient = mock(RefboxClient.class);
        when(refboxClient.isCyan()).thenReturn(true);
        when(refboxClient.isMagenta()).thenReturn(false);
        ReportMachineMsgHandler msgHandler = new ReportMachineMsgHandler(null, null, null, refboxClient);
        GripsExplorationReportMachineProtos.GripsExplorationReportMachine report = GripsExplorationReportMachineProtos.GripsExplorationReportMachine.newBuilder()
                .setRobotId(1)
                .setFoundMachine(true)
                .setMachineId("M-CS1")
                .setZone(ZoneProtos.Zone.M_Z27)
                .build();
        MachineReportProtos.MachineReportEntry mirrored = msgHandler.getReportInTeamColor(report);
        assertThat(mirrored.getName()).isEqualToIgnoringCase("C-CS1");
        assertThat(mirrored.getZone()).isEqualTo(ZoneProtos.Zone.C_Z27);
        GripsExplorationReportMachineProtos.GripsExplorationReportMachine report1 = GripsExplorationReportMachineProtos.GripsExplorationReportMachine.newBuilder()
                .setRobotId(1)
                .setFoundMachine(true)
                .setMachineId("C-CS2")
                .setZone(ZoneProtos.Zone.C_Z28)
                .build();
        MachineReportProtos.MachineReportEntry mirrored1 = msgHandler.getReportInTeamColor(report1);
        assertThat(mirrored1.getName()).isEqualToIgnoringCase("C-CS2");
        assertThat(mirrored1.getZone()).isEqualTo(ZoneProtos.Zone.C_Z28);
    }
}
