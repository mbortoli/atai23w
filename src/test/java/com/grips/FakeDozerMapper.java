package com.grips;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.util.Collections;

public class FakeDozerMapper {
    public static Mapper getMapper() {
        return new DozerBeanMapper(Collections.singletonList("mappings/order-mapping.xml"));
    }
}
